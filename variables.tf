variable "region" {
  description = "Region for hosting S3 bucket"
}

variable "domain" {
  description = "Fully Qualified Domain Name for the static website"
}
