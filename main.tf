provider "aws" {
  region = var.region
}

module "cdn" {
  source = "./modules/cdn"
  domain = var.domain
  cert_validation = module.network.cert_validation
  regional_domain_name = module.data.regional_domain_name
}

module "data" {
  source = "./modules/data"
  domain = var.domain
  oai_iam_arn = module.cdn.oai_iam_arn
}

module "network" {
  source = "./modules/network"
  domain = var.domain
  s3_domain_name = module.cdn.s3_domain_name
  s3_zone_id = module.cdn.s3_zone_id
  cert_record_name = module.cdn.cert_record_name
  cert_record_type = module.cdn.cert_record_type
  cert_record_value = module.cdn.cert_record_value
}
