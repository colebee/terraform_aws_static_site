Terraform - AWS Static Site Infrastructure
---
Outline for the infrastructure of a static website hosted in S3, served by Cloudfront to a domain registered on Route53.

**Root Input Variables**
- region: AWS region for hosting the S3 bucket [note: us-east-1 required for cert]
- domain: string domain name that is registered in Route53

## Modules
### CDN
The CDN module provides configuration for Cloudfront and the certificate registration.

**Inputs**
- domain
- aws\_route53\_record.cert\_validation.fqdn
- aws\_s3\_bucket.static\_site.bucket\_regional\_domain\_name

**Outputs**
- aws\_cloudfront\_origin\_access\_identity.oai.iam\_arn
- aws\_cloudfront\_distribution.s3\_dist.domain\_name
- aws\_cloudfront\_distribution.s3_dist.hosted\_zone\_id
- aws\_acm_certificate.cert.domain\_validation\_options.0.resource\_record\_name
- aws\_acm_certificate.cert.domain\_validation\_options.0.resource\_record\_type
- aws\_acm_certificate.cert.domain\_validation\_options.0.resource\_record\_value

### Data
The data module provides configuration for the S3 bucket to host the static site.

**Inputs**
- domain
- aws\_cloudfront\_origin\_access\_indentity.oai.iam\_arn

**Outputs**
- aws\_s3\_bucket.static\_site.bucket\_regional\_domain\_name

### Network
The network module provides configuration for setting up Route53 with associated records.

**Inputs**
- domain
- aws\_cloudfront\_distribution.s3\_dist.domain\_name
- aws\_cloudfront\_distribution.s3_dist.hosted\_zone\_id
- aws\_acm_certificate.cert.domain\_validation\_options.0.resource\_record\_name
- aws\_acm_certificate.cert.domain\_validation\_options.0.resource\_record\_type
- aws\_acm_certificate.cert.domain\_validation\_options.0.resource\_record\_value

**Outputs**
- aws\_route53\_record.cert\_validation.fqdn
