variable "domain" {
  description = "Fully Qualified Domain Name for the static website"
}

variable "oai_iam_arn" {
  description = "Origin Identity Access for S3 bucket"
}
