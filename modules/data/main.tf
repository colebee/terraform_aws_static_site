resource "aws_s3_bucket" "static_site" {
  bucket = "${var.domain}"
  acl    = "private"
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.static_site.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["${var.oai_iam_arn}"]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = ["${aws_s3_bucket.static_site.arn}"]

    principals {
      type        = "AWS"
      identifiers = ["${var.oai_iam_arn}"]
    }
  }
}

resource "aws_s3_bucket_policy" "apply_policy" {
  bucket = "${aws_s3_bucket.static_site.id}"
  policy = "${data.aws_iam_policy_document.s3_policy.json}"
}
