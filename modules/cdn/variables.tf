variable "domain" {
  description = "Fully Qualified Domain Name for the static website"
}

variable "cert_validation" {
  description = "Cert validation from Route53"
}

variable "regional_domain_name" {
  description = "S3 bucket regional domain name"
}
