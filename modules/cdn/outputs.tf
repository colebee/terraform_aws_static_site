output "oai_iam_arn" {
  value = aws_cloudfront_origin_access_identity.oai.iam_arn
}

output "s3_domain_name" {
  value = aws_cloudfront_distribution.s3_dist.domain_name
}

output "s3_zone_id" {
  value = aws_cloudfront_distribution.s3_dist.hosted_zone_id
}

output "cert_record_name" {
  value = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
}

output "cert_record_type" {
  value = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
}

output "cert_record_value" {
  value = aws_acm_certificate.cert.domain_validation_options.0.resource_record_value
}
