variable "domain" {
  description = "Fully Qualified Domain Name for the static website"
}

variable "s3_domain_name" {
  description = "Domain name attached to Cloudfront distribution"
}

variable "s3_zone_id" {
  description = "Cloudfront distribution zone ID"
}

variable "cert_record_name" {
  description = "Certification record name"
}

variable "cert_record_type" {
  description = "Certification record type"
}

variable "cert_record_value" {
  description = "Certification record value"
}
