resource "aws_route53_zone" "primary" {
  name = "${var.domain}"
}

resource "aws_route53_record" "static" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "${var.domain}"
  type    = "A"

  alias {
    name                   = "${var.s3_domain_name}"
    zone_id                = "${var.s3_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "cert_validation" {
  name    = "${var.cert_record_name}"
  type    = "${var.cert_record_type}"
  zone_id = "${aws_route53_zone.primary.zone_id}"
  records = ["${var.cert_record_value}"]
  ttl     = 60
}
